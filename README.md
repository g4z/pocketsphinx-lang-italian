
# Italian Language Pack for PocketSphinx

This repo holds a copy of the Italian language pack create by `Uberi` and posted on this Github issue. The code is unchanged by me and is just a mirror for my convienience.

See [this Github issue](https://github.com/Uberi/speech_recognition/issues/192) for more information.

## Installation

```
cd /tmp
git clone https://gitlab.com/g4z/pocketsphinx-lang-italian
cd pocketsphinx-lang-italian
unzip it-IT.zip
chown -R root:root pocketsphinx-data/it-IT
# Move it-IT folder to your pocketsphinx model folder
sudo mv pocketsphinx-data/it-IT /usr/local/share/pocketsphinx/model/
```


